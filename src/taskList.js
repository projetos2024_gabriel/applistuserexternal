import React, { useEffect, useState } from "react";
import { View, Button, Text, TouchableOpacity, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";

const TaskList = ({ navigation }) => {

        const [user, setUsers] = useState([]);
        useEffect(()=>{
            getUsers()
        },[])

        async function getUsers(){
            try{
               const response = await axios.get('https://jsonplaceholder.typicode.com/users');
               setUsers(response.data);
            }catch(error){
                console.error(error)
            }
        }
    

    
   
const taskPress=(user)=>{
    navigation.navigate('DetalhesDasTarefas',{user});
};

return (
    <View>
      <Text> Lista de Usuários </Text>
      <FlatList
        data={user}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
            <TouchableOpacity onPress={() => taskPress(item)} >
            <Text>{item.name}</Text>
        </TouchableOpacity>
          
        )}
      />
    </View>
  );
};
export default TaskList;

