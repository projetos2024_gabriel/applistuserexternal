import React from "react";
import { View, Button, Text, TouchableOpacity, FlatList } from "react-native";

const TaskDetails = ({ route }) => {
const {user} = route.params;

return(
    <View>
        <Text>Detalhes da Tarefa:</Text>
        <Text>Nome: {user.name}</Text>
        <Text>Sobrenome: {user.username}</Text>
        <Text>Telefone: {user.phone}</Text>
        <Text>Email: {user.email}</Text>
        <Text>Endereço: {user.address.city}</Text>
        <Text>Rua: {user.address.street}</Text>
        <Text>Complemento: {user.address.suite}</Text>
        <Text>CEP: {user.address.zipcode}</Text>
        <Text>Site: {user.website}</Text>
        <Text>Empresa: {user.company.name}</Text>



        
    </View>
)
}

export default TaskDetails;

    