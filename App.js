import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TaskList from './src/taskList';
import TaskDetails from './src/taskDetails';

const Stack = createStackNavigator();

export default function App() {
  return (
<NavigationContainer>
    <Stack.Navigator initialRouteName='Lista de Usuários'>
        <Stack.Screen name="Listagem Pessoal" component={TaskList}/>
        <Stack.Screen name="DetalhesDasTarefas" component={TaskDetails} options={{title: "Detalhes Pessoais"}} />
      </Stack.Navigator>
   </NavigationContainer>
  );
}
